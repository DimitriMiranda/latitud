import Maintenance from '../pageObjects/maintenance'
import LandmarksLocators from '../pageObjects/landmarks'

class LanmarksMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.landmarks = new LandmarksLocators
    }

    addLandmark(information){
        const info = information
        const maintenance = this.maintenance
        const landmarks = this.landmarks

        maintenance.addButton().click()
        landmarks.idInput().should('be.visible').wait(500).type(info.id)
        landmarks.timeZoneSelect().select(info.time_zone)
        landmarks.descriptionInput().type(info.description)
        landmarks.line1Input().type(info.address.line_1)
        landmarks.line2Input().type(info.address.line_2)
        landmarks.crossStreetInput().type(info.address.cross_street)
        landmarks.postalCodeInput().type(info.address.postal_code)
        landmarks.countrySelect().select(info.address.country)
        landmarks.division1Input().type(info.address.division_1)
        landmarks.division2Input().type(info.address.division_2)
        landmarks.division3Input().type(info.address.division_3)

        landmarks.saveButton().should('be.enabled').click()
        landmarks.closeButton().should('be.enabled').click()
    }

    editLandmark(information) { // Edits specific restricted location using id
        const landmarks = this.landmarks
        const maintenance = this.maintenance
        const info = information

        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == info.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    landmarks.descriptionInput().clear().type(info.description)
                    landmarks.line1Input().clear().type(info.address.line_1)
                    landmarks.line2Input().clear().type(info.address.line_2)
                    landmarks.crossStreetInput().clear().type(info.address.cross_street)
                    landmarks.postalCodeInput().clear().type(info.address.postal_code)
                    landmarks.countrySelect().select(info.address.country)

                    landmarks.division1Input().clear().type(info.address.division_1)
                    landmarks.division2Input().clear().type(info.address.division_2)
                    landmarks.division3Input().clear().type(info.address.division_3)

                    landmarks.saveButton().should('be.enabled').click()
                    return false
                }
            })
        })
    }

    deleteLandmark(locationId) { // Deletes an specific restricted location using id
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text().trim()
                cy.log(id)
                cy.log(locationId)
                if (id == locationId) {
                    cy.get($element).should('be.visible').wait(500).find('#deleteLink').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()

                    return false
                }
            })
        })
    }

    importLandmarks(filename) { //Imports mainteinance locations using xlsx file
        const maintenance = this.maintenance
        const landmarks = this.landmarks

        maintenance.importButton().click()
        landmarks.filePathInput().attachFile('Files/' + filename)

        landmarks.importButton().should('be.enabled').wait(500).click()
        landmarks.closeButton().should('be.enabled').click()
    }
}

export default LanmarksMethods