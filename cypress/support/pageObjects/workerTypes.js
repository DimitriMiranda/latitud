class WorkerTypesLocators{
    //Add Window
    idInput(){
        return cy.get('input#identifier')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    alertMessage(){
        return cy.get('#editMessageView')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }

}

export default WorkerTypesLocators