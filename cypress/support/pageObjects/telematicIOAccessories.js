class TelematicIOAccesoriesLocators{
    typeSelect(){
        return cy.get('select#accessoryType')
    }

    idInput(){
        return cy.get('input#identifier')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    binarySensorInputBiasSelect(){
        return cy.get('select#inputBias')
    }

    binarySensorVoltageHighLabel(){
        return cy.get('input#voltage-high-label')
    }

    binarySensorVoltageLowLabel(){
        return cy.get('input#voltage-low-label')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button#closeButton')
    }


}

export default TelematicIOAccesoriesLocators