class AdminGeneralLocators{
    currentStep(){
        return cy.get('#wizardCurrentStep')
    }

    addButton(){
        return cy.get('#addButton')
    }

    importButton(){
        return cy.get('#importButton')
    }

     getModules(){
        return cy.get('.dropdown.open li a')
    }

    elementList(){
        return cy.get('#scrollContainer > div > div.list-group-item > div')
    }

    deleteDialogYesButton(){
        return cy.get('#deleteDialog #yesButton')
    }

    deleteDialogNoButton(){
        return cy.get('#deleteDialog #noButton')
    }

}

export default AdminGeneralLocators
