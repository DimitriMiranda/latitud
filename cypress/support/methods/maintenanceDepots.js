import Maintenance from '../pageObjects/maintenance'
import DepotsLocators from '../pageObjects/depots'

class MaintenanceDepots{
    constructor(){
        this.maintenance = new Maintenance
        this.depots = new DepotsLocators
    }

    addDepot(information){
        const info = information
        const depots = this.depots
        const maintenance = this.maintenance

        maintenance.addButton().click()
        depots.idInput().should('be.visible').wait(500).type(info.address.id)
        depots.timeZoneDepotSelect().select(info.address.time_zone)
        depots.descriptionInput().type(info.address.description)
        depots.addressTab().click()
        depots.line1Input().type(info.address.line_1)
        depots.division3Input().type(info.address.division_3)
        depots.division2Input().type(info.address.division_2)
        depots.division1Input().type(info.address.division_1)
        depots.postalCodeInput().type(info.address.postal_code)
        depots.countrySelect().select(info.address.country)

        let details = info.details        
        if(details=!null){
            this.addDetails(info)            
        }                   
        
        this.depots.saveButton().click()
        this.depots.closeButton().click()
        
    }

    editDepot(info){
        const depots = this.depots
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==info.address.id){
                    cy.get($element).find('#editLink').should('be.visible').click()
                   
                    depots.timeZoneDepotSelect().select(info.address.time_zone)
                    depots.descriptionInput().clear().type(info.address.description)
                    depots.addressTab().click()
                    depots.line1Input().clear().type(info.address.line_1)
                    depots.division3Input().clear().type(info.address.division_3)
                    depots.division2Input().clear().type(info.address.division_2)
                    depots.division1Input().clear().type(info.address.division_1)
                    depots.postalCodeInput().clear().type(info.address.postal_code)
                    depots.countrySelect().select(info.address.country)
                    depots.saveButton().click()
                    // depots.closeButton().click()
                }
            })
        })
    }

    deleteDepot(depotId){
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==depotId){
                    cy.get($element).find('#deleteLink').click()                    
                    maintenance.deleteDialogYesButton().should('be.visible').click() 
                    
                }
            })
        })
    }


    addDetails(info){
        const depots = this.depots
        depots.detailsTab().click()
        depots.detailsPhone().should('be.visible').type(info.details.phone_number)
        depots.detailsAlternatePhone().type(info.details.alternate_number)
        depots.detailsFaxNumber().type(info.details.fax_number)
    }
}

export default MaintenanceDepots