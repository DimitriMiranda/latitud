import Maintenance from "../pageObjects/maintenance";
import MobileDevicesLocators from "../pageObjects/mobileDevices";

class MobileDevicesMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.mobileDevices = new MobileDevicesLocators
    }

    addMobileDevice(information){
        const info = information
        const maintenance = this.maintenance
        const mobileDevices = this.mobileDevices

        maintenance.addButton().click()
        mobileDevices.mobileDeviceIDInput().should('be.visible').type(info.details.id)
        mobileDevices.generateIDButton().click()
        mobileDevices.networkProviderSelect().select(info.details.network_provider)
        mobileDevices.deviceNumberInput().type(info.details.mobile_number)
        mobileDevices.descriptionInput().type(info.details.description)
        mobileDevices.mobilePlatfomSelect().select(info.details.platform)

        let apps = info.applications
        cy.log(apps)

        // if(apps!= null){
        //     mobileDevices.enableOmnitracsNavigationCheckbox().then($element =>{
                
        //     })
        // }

        mobileDevices.saveButton().should('be.enabled').click()
        mobileDevices.closeButton().should('be.enabled').click()     

    }

    editMobileDevice(information) { // Edits specific restricted location using id
        const mobileDevices = this.mobileDevices
        const maintenance = this.maintenance
        const info = information

        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == info.details.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    mobileDevices.descriptionInput().clear().type(info.details.description)
                    mobileDevices.deviceNumberInput().clear().type(info.details.mobile_number)

                    mobileDevices.saveButton().should('be.enabled').click()
                    return false
                }
            })
        })
    }

    importMobileDevices(filename) { //Imports mainteinance locations using xlsx file
        const maintenance = this.maintenance
        const mobileDevices = this.mobileDevices

        maintenance.importButton().click()
        mobileDevices.fileImportInput().attachFile('Files/' + filename)

        mobileDevices.importButton().should('be.enabled').wait(500).click()
        mobileDevices.closeButton().should('be.enabled').click()
    }

    deleteMobileDevice(mobileID) { // This method deletes an specific element using the provided id
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == mobileID) {
                    cy.get($element).find('#deleteLink').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()

                }
            })
        })
    }
}

export default MobileDevicesMethods