import CustomersLocators from "../../../support/pageObjects/AdminWeb/customers"
import LoginPageLocators from "../../../support/pageObjects/AdminWeb/loginPage"
import MainPageLocators from "../../../support/pageObjects/AdminWeb/mainPage"

import Login from "../../../support/pageObjects/loginPage"
import Customer from "../../../support/pageObjects/customerPage"
import Maintenance from "../../../support/pageObjects/maintenance"

describe('RPE-12846',function(){
    it('Test Legacy SSO option',()=>{
        let loginPage = new LoginPageLocators
        let mainPage = new MainPageLocators
        let customerModule = new CustomersLocators
        cy.server()
        cy.route('*/*')

        cy.visit(Cypress.env('ADMINWEB_URL'))
        
        loginPage.usernameInput().type('dimitri.miranda@omnitracs.com')
        loginPage.passwordInput().type('Roadnet@Mikau1401')
        loginPage.signInButton().click()

        mainPage.modulesList().each(function($element,$index){
            let moduleText = $element.text()           
            if(moduleText.includes('Customers')){
                cy.log('Customers')
               cy.request(Cypress.env('ADMINWEB_URL')+'/Customers')
               cy.visit(Cypress.env('ADMINWEB_URL')+'/Customers')
            }
        })
        
        cy.pause()
        // .then(function(){
        //     customerModule.addButton().should('contain.text','Add')
        // })

        
    })

    // it('Customer page',()=>{
    //     let login = new Login
    //     let customer = new Customer
    //     let maintenance = new Maintenance

    //     cy.visit(Cypress.env('DEV_URL'))
    //     login.usernameInput().type('dimitri.miranda@omnitracs.com')
    //     login.passwordInput().type('Roadnet@Mikau1401')
    //     login.submitButton().click()

    //     customer.customerSelect().select('Dimitri Test 02')
    //     customer.changeCustomer().click()

    //     customer.moduleList().each(function($element, $index){
    //         let moduleText = $element.text()
    //         if(moduleText.includes('Maintenance')){
    //             cy.log('Maintenance')
    //             customer.moduleList().eq($index).find('a').click({force:true})
    //             return false
    //         }
    //     })
    // })
})