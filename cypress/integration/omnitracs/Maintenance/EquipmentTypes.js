import Login from '../../../support/methods/loginPage'
import Customer from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import EquipmentTypesMethods from "../../../support/methods/maintenanceEquipmentTypes"

describe('Equipment Types',function(){
    before('Login and move to module',function(){
        this.login = new Login
        this.customer = new Customer
        this.maintenance = new MaintenanceModule
        this.equipmentTypes = new EquipmentTypesMethods
        

        cy.fixture('Credentials/customerLatitud').then(function($credentials){
            this.login.visitLatitud()
            this.login.loginLatitud($credentials.username,$credentials.password)
        })

        cy.fixture('Navigation/equipment').then(function($equipment){
            this.customer.selectCustomer($equipment.customer)
            this.customer.accessModule($equipment.module)
            this.maintenance.accessToModule('Equipment Types')
        })

        cy.fixture('Maintenance/EquipmentTypes/addInfo').then(function($addInfo){
            this.addInfo = $addInfo
        })

        cy.fixture('Maintenance/EquipmentTypes/editInfo').then(function($editInfo){
            this.editInfo = $editInfo
        })
    })

    it('Test Equipment Type',function(){
        this.equipmentTypes.editEquipmentType(this.editInfo)
        cy.wait(3000)
        this.equipmentTypes.deleteEquipmentType(this.addInfo.id)
    })
})