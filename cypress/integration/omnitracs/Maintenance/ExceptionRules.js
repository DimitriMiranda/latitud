import ExceptionRulesMethods from '../../../support/methods/maintenanceExceptionRules'
import MaintenanceModule from '../../../support/methods/Maintenance'
import LoginPage from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'

describe("Test Exception Rules",function(){
    before('Login latitud',function(){  
        
        //methods
        this.loginPage = new LoginPage
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.exceptionRules = new ExceptionRulesMethods
    
        cy.clearCookies()
        this.loginPage.visitLatitud()
        
    
        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
           cy.get('body').then(($body)=>{ // This is a test code to avoid the walk me pop up
               if($body.find('path')>0){
                   cy.get('path').click()
               }
           })
            
        })
    
        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Exception Rules')
        })
    
        //Add Service Location
        cy.fixture('Maintenance/ExceptionRules/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

    })

    it("Add Exception rule",function(){
        this.exceptionRules.addExceptionRule(this.addInfo)
    })
})