import LoginPage from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import MobileDevicesMethods from '../../../support/methods/maintenanceMobileDevices'

describe('Test Mobile Devices',function(){
    before('Login, Access Customer, Access Module',function(){

        //methods
        this.loginPage = new LoginPage
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.mobileDevices = new MobileDevicesMethods
    
        cy.clearCookies()
        this.loginPage.visitLatitud()
        
    
        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
        //    cy.get('body').then(($body)=>{ // This is a test code to avoid the walk me pop up
        //        if($body.find('path')>0){
        //            cy.get('path').click()
        //        }
        //    })
            
        })
    
        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Mobile Devices')
        })
    
        //Add Service Location
        cy.fixture('Maintenance/MobileDevice/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

        cy.fixture('Maintenance/MobileDevice/editInfo').then(function ($editInfo) {
            this.editInfo = $editInfo
        })



    })

    it('Add mobile device',function(){
        this.mobileDevices.addMobileDevice(this.addInfo)
        cy.wait(3000)
        this.mobileDevices.editMobileDevice(this.editInfo)
        cy.wait(3000)
        this.mobileDevices.importMobileDevices('MobileDevices.xlsx')
        cy.wait(3000)
        this.mobileDevices.deleteMobileDevice('IMPTEST002')
    })

    
})