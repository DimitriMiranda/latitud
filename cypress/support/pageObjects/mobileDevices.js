class MobileDevicesLocators{

    //Add Window

    //Tabs
    detailsTab(){
        return cy.get('ul#MobileDevice-tabs li:contains("Details")')
    }

    applicationsTab(){
        return cy.get('ul#MobileDevice-tabs li:contains("Applications")')
    }

    sharedByRegionsTab(){
        return cy.get('ul#MobileDevice-tabs li:contains("Shared By Regions")')
    }

    //Details Tab
    activeDeviceCheckbox(){
        return cy.get('input#active')
    }

    networkProviderSelect(){
        return cy.get('select#mobileNetworkProvider')
    }

    mobileDeviceIDInput(){
        return cy.get('input#identifier')
    }

    generateIDButton(){
        return cy.get('button:contains("Generate ID"):visible')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    deviceNumberInput(){
        return cy.get('input#devicePhoneNumber')
    }

    mobilePlatfomSelect(){
        return cy.get('select#mobilePlatform')
    }

    //Aplication Tab

    enableOmnitracsNavigationCheckbox(){
        return cy.get('input#enableOmnitracsNavigation')
    }

    preAuthenticatedCheckbox(){
        return cy.get('label:contains("Pre-Authenticated") input')
    }


    //Import window

    fileImportInput(){
        return cy.get('input#file-input-importFileInput')
    }

    //Save, Import and Close buttons

    importButton(){
        return cy.get('button#importExecuteButton')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }








}

export default MobileDevicesLocators