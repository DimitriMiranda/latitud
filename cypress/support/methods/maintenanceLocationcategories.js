import LocartionCategoriesLocators from "../pageObjects/locationCategories"
import Maintenance from "../pageObjects/maintenance"

class LocationCategoriesMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.locationCategories = new LocartionCategoriesLocators
    }

    addLocationCategory(information){
        const info = information
        const maintenance = this.maintenance
        const locationCategories = this.locationCategories

        maintenance.addButton().should('be.visible').click()
        locationCategories.idInput().should('be.visible').wait(500).type(info.id)
        locationCategories.displayColorButton().wait(500).click()
        locationCategories.colorOptions().eq(5).click()
        locationCategories.descriptionInput().type(info.description)

        locationCategories.saveButton().should('be.enabled').click()
        locationCategories.closeButton().should('be.enabled').click()
    }

    editLocationCategory(information){
        const info = information
        const maintenance = this.maintenance
        const locationCategories = this.locationCategories
        let found

        maintenance.equipmentElementList().each(function ($element, $index) {
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement) => {
                if ($innerElement.text() == info.id) {
                    found = $index
                    return false
                }
            })
        }).then(() => {
           maintenance.equipmentElementList().eq(found).find('#editLink').click()
           locationCategories.idInput().should('be.visible')
           locationCategories.descriptionInput().clear().wait(500).type(info.description)
           locationCategories.saveButton().should('be.enabled').click()
        })

    }

    deleteLocationCategory(locationCategoryId){
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==locationCategoryId){
                    cy.get($element).find('#deleteLink').should('be.visible').click()                    
                    maintenance.deleteDialogYesButton().should('be.visible').click() 
                    
                }
            })
        })
        // const maintenance = this.maintenance
        // let found = 0
        // maintenance.equipmentElementList().each(function($element,$index){
        //     cy.get($element).find('span.entity-name').eq(0).then(($innerElement)=>{
        //         if($innerElement.text() == workerTypeId){
        //             found = $index
        //         }
        //     })
        // }).then(()=>{
        //     maintenance.equipmentElementList().eq(found).find('#deleteLink').should('be.visible').wait(3000).click().then(()=>{
        //         maintenance.deleteDialogYesButton().should('be.visible').click()
        //     })
        // })
    }

}

export default LocationCategoriesMethods