class EquipmentTypesLocators{
    //Add Window
    idInput(){
        return cy.get('input#identifier')
    }

    //Details Tab
    descriptionInput(){
        return cy.get('input#description')
    }

    classSelect(){
        return cy.get('select#equipmentClass')
    }

    fixedCostInput(){
        return cy.get('#fixedCost input')
    }

    variableCostInput(){
        return cy.get('#variableCost input')
    }

    heightInput(){
        return cy.get('#height input')
    }

    lengthInput(){
        return cy.get('#length input')
    }

    widthInput(){
        return cy.get('#width input')
    }

    weightInput(){
        return cy.get('#weight input')
    }

    IdlingFuelUsagePerHourInput(){
        return cy.get('#IdlingFuelUsagePerHour input')
    }

    powerUnitTypeRadio(type){
        return cy.get('input#powerUnit'+type)
    }

    restructedCommertialRoadsCheckbox(){
        return cy.get('input#restrictedCommercialRoads')
    }











    
    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }
}

export default EquipmentTypesLocators