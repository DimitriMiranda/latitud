import Maintenance from '../pageObjects/maintenance'
import EquipmentLocators from '../pageObjects/equipment'

class MaintenanceEquipment{
    constructor(){
        this.maintenanceLocators = new Maintenance
        this.equipmentLocators = new EquipmentLocators
    }

    importFile(fileName) {
        this.maintenanceLocators.importButton().click()
        this.equipmentLocators.inputFilePath().attachFile('Files/' + fileName)
        this.equipmentLocators.importFileButton().should('be.enabled').click()
        this.equipmentLocators.importCloseButton().should('be.enabled').click()
    }

    addEquipment(equipmentInfo, validation) {
        this.maintenanceLocators.addButton().click()
        cy.wait(3000)
        this.equipmentLocators.equipmentInputId().as('inputId').type(equipmentInfo.id)
        this.equipmentLocators.equipmentInputDescription().type(equipmentInfo.description)
        this.equipmentLocators.equipmentSelectEquipmentType().as('SelectEquipment').find('option').each(function ($element) {
            if ($element.text().includes(equipmentInfo.equipment_type)) {
                let selectValue = $element.prop('value')
                cy.get('@SelectEquipment').select(selectValue)
            }
        })
        this.equipmentLocators.equipmentTelematicDeviceInput().type('DIM')
        this.equipmentLocators.telematicElementList().each(function($element){
            let teleId = $element.text()
            if(teleId.includes('DIMEQ004')){
                cy.get($element).click()
                return false
            }
                
        })
        this.equipmentLocators.saveButton().should('be.enabled').click()
        this.equipmentLocators.closeButton().click()

        //Validation 
        if (validation) {
            this.maintenanceLocators.equipmentElementList().as('elementList').its('length').then(function ($len) {
                this.equipmentLenght = $len
            }).then(function () {
                cy.get('@elementList').eq(this.equipmentLenght - 1).find('span.entity-name').eq(0).then(function ($element) {
                    let id = $element.text()
                    expect(id).to.equal(equipmentInfo.id)
                })
            })
        }
    }

    editEquipment(equipmentInfo) {
        this.maintenanceLocators.equipmentElementList().as('elementList').each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == equipmentInfo.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    const eq = new EquipmentLocators
                    eq.equipmentInputDescription().as('description').should('be.visible').clear().then(function () { cy.get('@description').type(equipmentInfo.description) })
                    eq.equipmentSelectEquipmentType().as('SelectEquipment').find('option').each(function ($element) {
                        if ($element.text().includes(equipmentInfo.equipment_type)) {
                            let selectValue = $element.prop('value')
                            cy.get('@SelectEquipment').select(selectValue)
                        }
                    })
                }
            })
        })
        this.equipmentLocators.saveButton().should('be.enabled').click()
        this.equipmentLocators.closeButton().click()
    }

    deleteEquipment(equipmentId) {
        this.maintenanceLocators.equipmentElementList().as('elementList').each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == equipmentId) {
                    cy.get($element).find('#deleteLink').click({ force: true })
                    const ml = new Maintenance
                    ml.deleteDialogYesButton().should('be.visible').click()
                }
            })
        })
    }

}

export default MaintenanceEquipment