import MaintenanceLocations from '../../../support/methods/maintenanceMaintenanceLocations'
import MaintenanceModule from '../../../support/methods/maintenance'
import LoginPage from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'

describe('Maintenance Location Tests',function(){
    before('Login latitud',function(){
  
        //methods
        this.loginPage = new LoginPage
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.maintenanceLocations = new MaintenanceLocations
    
    
        cy.clearCookies()
        cy.clearLocalStorage()
        this.loginPage.visitLatitud()
        
    
        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
            // this.loginPage.loginLatitud(Cypress.env("PROD_USR"), $credentials.password)

            cy.get('#changeCustomer').should('be.visible')

            // cy.get('body').then(($body)=>{ 
            //     if($body.find('div[id*="walkme-visual-design"]')>0){
            //         cy.get($walkme,{timeout:30000}).find('button').eq(1).click()
            //     }
            // })

            // cy.get('div[id*="walkme-visual-design"]').then(function($walkme){ // This is the code to close the walkme window
            //     cy.get($walkme).find('button').eq(1).click()
                
            // }) 
        })
    
        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Maintenance Locations')
        })
    
        //Add Service Location
        cy.fixture('Maintenance/MaintenanceLocation/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

        
    })

 

    it('Maintenance location methods',function(){
        const maintenanceLocation = this.maintenanceLocations
        maintenanceLocation.importMaintenanceLocations('MaintenanceLocations.xlsx')
        // cy.wait(1000)
        // maintenanceLocation.addMaintenanceLocation(this.addInfo)
        // cy.wait(1000)
        // maintenanceLocation.deleteMaintenanceLocation('IMPML001')
        // cy.wait(1000)
        // maintenanceLocation.editMaintenanceLocation(this.addInfo)
    })
})