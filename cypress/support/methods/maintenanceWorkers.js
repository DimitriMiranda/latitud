import Maintenance from '../pageObjects/maintenance'
import WorkersLocators from '../pageObjects/workers'

class MaintenanceWorkers{
    constructor(){
        this.workersLocators = new WorkersLocators
    }

    addWorker(info) {
        const maintenanceLocators = new Maintenance

        maintenanceLocators.addButton().should('be.visible').click()
        cy.wait(3000)

        this.addWorkerDetails(info)

        
        
        // this.workersLocators.saveButton().should('be.enabled').click()
        // this.workersLocators.closeButton().click()

       
    }

    addWorkerDetails(info){
        const workersLocators = this.workersLocators       
        workersLocators.workerIdInput().should('be.visible').type(info.details.id)
        workersLocators.colorDisplayButton().click()
        cy.wait(500)
        workersLocators.colorList().eq(1).click() // Missing code for managing color
        workersLocators.workerFirstNameInput().type(info.details.first_name)
        workersLocators.workerLastNameInput().type(info.details.last_name)
        // workersLocators.workerTypeSelect().select(info.details.worker_type) Require method to select workerType
        // workersLocators.workerEquipmentSelect().select(info.details.equipment)
        // workersLocators.workerDepotSelect().select(info.details.depot)
        workersLocators.workerMobileAppSelect().select(info.details.mobile_app)
        workersLocators.workerContactPhInput().type(info.details.contact_phone)
        if(info.details.compliance){
            workersLocators.complianceCheckbox().siblings('input[type="checkbox"]').check()
            if(info.details.mobile_id!=null){workersLocators.mobileLoginInput().clear().wait(500).type(info.details.mobile_id)}
            if(info.details.key_fob!=null){workersLocators.workerKeyFobInput().clear().wait(500).type(info.details.key_fob)}
            workersLocators.mobilePasswordInput().type(info.details.mobile_password)
            workersLocators.mobilePasswordConfirmationInput().type(info.details.mobile_password)
            if(info.compliance!=null){this.addWorkerCompliance(info)}            
            
        }else{
            workersLocators.complianceCheckbox().siblings('input[type="checkbox"]').uncheck()
        }
        
        this.addWorkerSharedToRegions(info)
    }

    addWorkerCompliance(info){
        const workersLocators =this.workersLocators
        workersLocators.complianceTab().scrollIntoView().wait(100).click({force:true})
        workersLocators.licenseNumberInput().type(info.compliance.license_number)
        workersLocators.licenseCountrySelect().select(info.compliance.license_country)
        workersLocators.licenseStateSelect().select(info.compliance.license_state)
    }

    addWorkerSharedToRegions(info){
        const workersLocators = this.workersLocators
        workersLocators.sharedByRegionsTab().scrollIntoView().click({force:true})
        if(info.shared_to_regions.shared_to_all){
            workersLocators.sharedToAllCheckbox().check()
        }else{
            workersLocators.sharedToAllCheckbox().uncheck()
            //Code for each region selection
        }
    }



    editWorker() {
        this.maintenanceLocators.equipmentElementList().as('elementList').each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == "WORKID") {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    const wk = new WorkersLocators
                    wk.workerFirstNameInput().clear().type("ITZELT")
                    wk.workerLastNameInput().clear().type("HUERTA")
                    wk.workerContactPhInput().clear().type("5589632145")
                    
                }
            })
        })
        this.workersLocators.saveButton().should('be.enabled').click()
        cy.wait(3000)
        // this.maintenanceLocators.closeButton().click()
    }

    deleteWorker() {        
        this.maintenanceLocators.equipmentElementList().as('elementList').each( ($element)=> {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == "DIMEQ005") {
                    cy.get($element).find('#deleteLink').click({ force: true })
                    const ml = new Maintenance
                    ml.deleteDialogYesButton().should('be.visible').click()                                                   
                }
            })          
        })
    }

    importWorker(fileName){
        this.maintenanceLocators.importButton().click()
        this.workersLocators.importFileInput().attachFile('Files/'+fileName,{force:true})
        this.workersLocators.importFileButton().should('be.enabled').click()
        this.workersLocators.importCloseButton().should('be.enabled').click()
        cy.wait(2000)
        
    }

}

export default MaintenanceWorkers