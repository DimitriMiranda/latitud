/// <reference types="Cypress" />

import Login from '../../../Pages/LoginPage.js'
import Customer from '../../../Pages/CustomerPage.js'

describe('Validate translation to French',function(){

    var loginPage = new Login
    var customerPage = new Customer

    it('Login Page',function(){

      console.log(Cypress.env('lang'))
      loginPage.verifyTitle('Bienvenue')
    
    })

    it('Welcome Page',function(){

      cy.fixture('Credentials/customerLatitud').then(function($credentials){
          loginPage.loginLatitud($credentials.username, $credentials.password)  
          customerPage.validateCustomerMessage("Changer de client")
      
      })

    })


})