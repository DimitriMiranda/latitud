import Login from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import MaintenanceWorkers from '../../../support/methods/maintenanceWorkers'

describe("Tests Workers",function(){
    before(function(){
        this.login = new Login
        this.customer = new CustomerPage
        this.maintenance = new MaintenanceModule
        this.workers = new MaintenanceWorkers
        
        this.login.visitLatitud()

        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.login.loginLatitud($credentials.username, $credentials.password)
        })

        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customer.selectCustomer($navigation.customer)
            this.customer.accessModule($navigation.module)
            this.maintenance.accessToModule("Workers")
        })
    })

    it("Edit Worker",function(){
        this.workers.importWorker('ImportWorker.xlsx')
        this.workers.addWorker()
        this.workers.editWorker()
        this.workers.deleteWorker()
        
    })
})
