class TelematicDevicesLocators{

    //Tabs
    detailsTab(){
        return cy.get('ul#TelematicsDevice-tabs li:contains("General")')
    }

    applicationsTab(){
        return cy.get('ul#TelematicsDevice-tabs li:contains("Applications")')
    }

    sharedByRegionsTab(){
        return cy.get('ul#TelematicsDevice-tabs li:contains("Shared By Regions")')
    }

    //General Tab
    activeDeviceCheckbox(){
        return cy.get('input#active')
    }

    thirdPartyDeviceCheckbox(){
        return cy.get('input#isThirdPartyDevice')
    }

    hardwarePlatformSelect(){
        return cy.get('select#hardwarePlatform')
    }

    telematicProviderDeviceSelect(){
        return cy.get('select#telematicsProviderType')
    }

    telematicIDInput(){
        return cy.get('input#identifier')
    }

    telematicDescriptionInput(){
        return cy.get('input#description')
    }

    //Applications Tab
    enableOmnitracsNavigationChackbox(){
        return cy.get('input#enableOmnitracsNavigation')
    }

    preAuthenticatedCheckbox(){
        return cy.get('label:contains("Pre-Authenticated") input')
    }

    //Shared By Regions Tab
    sharedToAllRegionsCheckbox(){
        return cy.get('input#sharedToAll')
    }

    //Import window

    importFileInput(){
        return cy.get('input#file-input-importFileInput')
    }

    //Import, Add and Close buttons
    importButton(){
        return cy.get('button#importExecuteButton')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }


}

export default TelematicDevicesLocators