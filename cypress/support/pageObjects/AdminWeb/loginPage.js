class LoginPageLocators{
    usernameInput(){
        return cy.get('input#UserName')
    }

    passwordInput(){
        return cy.get('input#Password')
    }

    signInButton(){
        return cy.get('button#submitButton')
    }

}

export default LoginPageLocators