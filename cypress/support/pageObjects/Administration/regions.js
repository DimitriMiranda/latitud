import AdminGeneralLocators from '../Administration/admin'
class RegionLocators extends AdminGeneralLocators{
    hosSideMenuLabel(){
        return cy.get('label:contains("HOS Settings")')
    }

    //HOS Settings options
    rulesOptionsOptionLabel(){
        return cy.get('label:contains("Rules and Options")')
    }

    //Rules and options elements
    promptForOffDutyAtLogoutCheckbox(){
        return cy.get('input#promptForOffDutyAtLogout')
    }
    

}

export default RegionLocators