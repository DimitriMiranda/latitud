class ExceptionRulesLocators{

    //Tabs
    generalTab(){
        return cy.get('ul#ResourceExceptionRuleEx-tabs li:contains("General") a')
    }

    generalTab(){
        return cy.get('ul#ResourceExceptionRuleEx-tabs li:contains("Recipients") a')
    }

    generalTab(){
        return cy.get('ul#ResourceExceptionRuleEx-tabs li:contains("Shared By Regions") a')
    }

    //General Tab
    idInput(){
        return cy.get('input#identifier')
    }

    ruleTypeSelect(){
        return cy.get('select#ruleType')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    displayColorButton(){
        return cy.get('#displayColor span i')
    }

    colorList(){
        return cy.get('#displayColor ul li')
    }

    requireAcknowledgementCheckbox(){
        return cy.get('input#requiresAcknowledgement')
    }

    enabledCheckbox(){
        return cy.get('input#isEnabled')
    }

    lowBatteryThresholdInput(){ //Element when Device Low Battery selected
        return cy.get('div#batteryLevel input')
    } 

    maxDistanceWithoutGPSInput(){//Element when GPS Gap selected
        return cy.get('div#maxDistanceWithoutGps input')
    }

    idlingDurationInput(){//Element visible when Idling selected
        return cy.get('input#inputduration')
    }

    outOfPlanDurationInput(){//Element visible when off planned selected
        return cy.get('input#inputoffPlanThreshold')
    }

    outOfBoundsInput(){//Element visible when Out of Bounds selected
        return cy.get('div#outOfBoundsDistance input')
    }

    outOfContactInput(){//Element visible when out of contact selected
        return cy.get('input#inputoutOfContactThreshold')
    }

    speedViolationMilesToleranceInput(){//Element visible when Posted Road Speed Violation selected
        return cy.get('div#overLimitTolerance input')
    }

    speedViolationDurationInput(){//Element visible when Posted Road Speed Violation selected
        return cy.get('input#inputminimumDuration')
    }

    earlyMissedServiceWindowMinutesInput(){//Element visible when Projected Missed Window selected
        return cy.get('div#earlyServiceWindowBuffer input')
    }

    lateMissedServiceWindowMinutesInput(){//Element visible when Projected Missed Window selected
        return cy.get('div#lateServiceWindowBuffer input')
    }

    missedWindowIgnoreEarlyArrivalsInput(){//Element visible when Projected Missed Window selected
        return cy.get('input#ignoreEarlyArrivals')
    }

    restrictedEquipmentHoursModeSelect(){//Element visible when Restricted Equipment Hours selected
        return cy.get('select#restrictedEquipmentHoursMode')
    }

    routePathDeviationMilesInput(){//Element visible when Route Path Deviation selected
        return cy.get('div#routePathDeviationThreshold input')
    }

    serviceTimeDeviationVarianceMinutes(){//Element visible when Service Time Deviation selected
        return cy.get('div#serviceTimeDeviationLimit input')
    }

    serviceTimeDeviationPercentage(){//Element visible when Service Time Deviation selected
        return cy.get('div#serviceTimeDeviationPercentage input')
    }

    uplannedStopMaximumValueInput(){
        return cy.get('input#inputtimeLimit')
    }

    userDefinedSpeedViolationMaxSpeedInput(){
        return cy.get('div#speedLimit input')
    }

    userDefinedSpeedViolationMinDurationInput(){
        return cy.get('input#inputminimumDuration')
    }

    notificationsEnabledCheckbox(){
        return cy.get('input#notificationsEnabled')
    }

    emailRecipientsTab(){
        return cy.get('ul.nav.nav-tabs li:contains("Email Recipients")')
    }

    emailRecipientsTab(){
        return cy.get('ul.nav.nav-tabs li:contains("Text Message Recipients")')
    }

    emailAddressInput(){
        return cy.get('input#emailRecipientIdentifierInput')
    }

    emailNameInput(){
        return cy.get('input#emailRecipientNameInput')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }


















}

export default ExceptionRulesLocators