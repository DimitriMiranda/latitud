import Login from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import MaintenanceDepots from '../../../support/methods/maintenanceDepots'

describe('Tests Depots', function () {
    before(function () {
        //methods
        this.loginPage = new Login
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.depots = new MaintenanceDepots

        this.loginPage.visitLatitud()

        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
        })

        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Depots')
        })

        //Add Depot
        cy.fixture('Maintenance/Depot/addDepot').then(function ($addInfo) {
            this.addInfo = $addInfo
        })


    })

    it('Depots functionalities (add, edit, delete)', function () {
        this.depots.addDepot(this.addInfo)
        this.depots.editDepot(this.addInfo)
        this.depots.deleteDepot(this.addInfo.address.id)
    })
})
