class LocartionCategoriesLocators{
    idInput(){
        return cy.get('input#identifier')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    displayColorButton(){
        return cy.get('div.form-control.colorPickerInput span i')
    }

    colorOptions(){
        return cy.get('ul.dropdown-menu.colorPickerDropdown li')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button#closeButton')
    }


}

export default LocartionCategoriesLocators