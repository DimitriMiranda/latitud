import Maintenance from '../pageObjects/maintenance'
import ServiceLocationLocators from '../pageObjects/serviceLocations'

class ServiceLocations {
    constructor() {
        this.maintenance = new Maintenance
        this.serviceLocations = new ServiceLocationLocators
    }


    importServiceLocationFile(fileName) { // This method import a file of service locations
        const maintenance = this.maintenance
        const serviceLocations = this.serviceLocations

        maintenance.importButton().click()
        serviceLocations.coordinateFormatSelect().select('DecimalDegree')
        serviceLocations.importFileInput().attachFile('Files/' + fileName)
        serviceLocations.importButton().should('be.enabled').click()
        serviceLocations.closeButton().should('be.enabled').click()
    }

    addServiceLocation(information) { // This method add a new service location
        const serviceLocations = this.serviceLocations
        const maintenance = this.maintenance
        const info = information

        maintenance.addButton().click()
        serviceLocations.idInput().should('be.visible').type(info.address.id)
        serviceLocations.descriptionInput().type(info.address.description)
        serviceLocations.timeZoneSelect().select(info.address.time_zone)
        serviceLocations.externalAddressInput().type(info.address.line_1)
        serviceLocations.internalAddressInput().type(info.address.line_2)
        serviceLocations.crossStreetInput().type(info.address.cross_street)
        serviceLocations.postalCodeInput().type(info.address.postal_code)
        serviceLocations.countrySelect().select(info.address.country)
        serviceLocations.division1Input().type(info.address.division_1)
        serviceLocations.division2Input().type(info.address.division_2)
        serviceLocations.division3Input().type(info.address.division_3)

        let details = info.details
        if (details != null) {
            this.addDetails(info)
        }

        serviceLocations.saveButton().should('be.enabled').click()
        serviceLocations.closeButton().click()
    }

    editServiceLocation(info) { // This method edits a service location
        const serviceLocations = this.serviceLocations
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == info.address.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    serviceLocations.descriptionInput().should('be.visible').clear().type(info.address.description)
                    serviceLocations.timeZoneSelect().select(info.address.time_zone)
                    serviceLocations.externalAddressInput().clear().type(info.address.line_1)
                    serviceLocations.internalAddressInput().clear().type(info.address.line_2)
                    serviceLocations.crossStreetInput().clear().type(info.address.cross_street)
                    serviceLocations.postalCodeInput().clear().type(info.address.postal_code)
                    serviceLocations.countrySelect().select(info.address.country)
                    serviceLocations.division1Input().clear().type(info.address.division_1)
                    serviceLocations.division2Input().clear().type(info.address.division_2)
                    serviceLocations.division3Input().clear().type(info.address.division_3)
                    serviceLocations.saveButton().click()
                }

            })
        })
    }

    deleteServiceLocation(serviceId) { // This method deletes an specific element using the provided id
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == serviceId) {
                    cy.get($element).find('#deleteLink').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()

                }
            })
        })
    }

    addDetails(information) { // This method works within add service location for addin details tab
        const info = information
        const serviceLocations = this.serviceLocations

        serviceLocations.detailsTab().click()
        serviceLocations.mainContactFirstNameInput().should('be.visible').type(info.details.main_contact_first_name)
        serviceLocations.mainContactLastNameInput().type(info.details.main_contact_last_name)
        serviceLocations.mainContactPhoneNumber().type(info.details.main_contact_phone_number)
        serviceLocations.alternateContactFirstNameInput().type(info.details.alternate_contact_first_name)
        serviceLocations.alternateContactLastNameInput().type(info.details.alternate_contact_last_name)
        serviceLocations.alternateContactPhoneNumber().type(info.details.alternate_contact_phone_number)
    }

    clearServiceLocations() {  // This method deletes all elements of service location
        const maintenance = this.maintenance
        maintenance.equipmentElementList().as('elements').its('length').then(($len)=>{
            let length = Number($len)
            while(length>0){
                cy.get('@elements').eq(0).should('be.visible').find('#deleteLink').click()                    
                maintenance.deleteDialogYesButton().should('be.visible').click()
                cy.wait(4000)
                length=length-1
            }
        })
    }

}

export default ServiceLocations