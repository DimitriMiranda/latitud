import Maintenance from "../pageObjects/maintenance"
import WorkerTypesLocators from "../pageObjects/workerTypes"

class WorkerTypesMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.workerTypes = new WorkerTypesLocators
    }

    addWorkerType(information){
        const info = information
        const maintenance = this.maintenance
        const workerTypes = this.workerTypes

        maintenance.addButton().should('be.visible').wait(500).click()
        workerTypes.idInput().should('be.visible').wait(500).type(info.id)
        workerTypes.descriptionInput().type(info.description)
        workerTypes.saveButton().should('be.enabled').click()
        workerTypes.closeButton().should('be.enabled').click()
    }

    editWorkerType(information){
        const info = information
        const maintenance = this.maintenance
        const workerTypes = this.workerTypes
        let found

        maintenance.equipmentElementList().each(function ($element, $index) {
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement) => {
                if ($innerElement.text() == info.id) {
                    found = $index
                    return false
                }
            })
        }).then(() => {
            maintenance.equipmentElementList().eq(found).find('#editLink').click()
            workerTypes.idInput().should('be.visible')
            workerTypes.descriptionInput().clear().wait(500).type(info.description)
            workerTypes.saveButton().should('be.enabled').click()
           
        })
    }

    deleteWorkerType(workerTypeId){
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==workerTypeId){
                    cy.get($element).find('#deleteLink').should('be.visible').click()                    
                    maintenance.deleteDialogYesButton().should('be.visible').click() 
                    
                }
            })
        })
        // const maintenance = this.maintenance
        // let found = 0
        // maintenance.equipmentElementList().each(function($element,$index){
        //     cy.get($element).find('span.entity-name').eq(0).then(($innerElement)=>{
        //         if($innerElement.text() == workerTypeId){
        //             found = $index
        //         }
        //     })
        // }).then(()=>{
        //     maintenance.equipmentElementList().eq(found).find('#deleteLink').should('be.visible').wait(3000).click().then(()=>{
        //         maintenance.deleteDialogYesButton().should('be.visible').click()
        //     })
        // })
    }
}

export default WorkerTypesMethods