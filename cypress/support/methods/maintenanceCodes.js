import CodesLocators from '../pageObjects/codes'
import Maintenance from '../pageObjects/maintenance'

class CodesMethods{
    addCode(information){
        const info = information
        const maintenance = new Maintenance
        const codes = new CodesLocators

        maintenance.addButton().click()
        codes.idInput().should('be.visible').wait(500).type(info.id)
        codes.descriptionInput().type(info.description)

        codes.saveButton().should('be.enabled').click()
        codes.closeButton().should('be.enabled').click()
    }

    editCode(information){
        const info = information
        const maintenance = new Maintenance
        const codes = new CodesLocators

        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==info.id){
                    cy.get($element).find('#editLink').click().then(function(){
                        codes.descriptionInput().clear().wait(500).type(info.description)
                        codes.saveButton().should('be.enabled').click()
                        return false
                    })
                }

            })
        })
    }

    deleteCode(codeId){
        const maintenance = new Maintenance

        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                if($innerElement.text()==codeId){
                    cy.get($element).find('#deleteLink').click().then(function(){
                        maintenance.deleteDialogYesButton().should('be.visible').click()
                    })
                }
            })
        })
    }
}

export default CodesMethods