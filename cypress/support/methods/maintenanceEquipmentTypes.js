import EquipmentTypesLocators from "../pageObjects/equipmentTypes"
import Maintenance from "../pageObjects/maintenance"

class EquipmentTypesMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.equipmentTypesLocators = new EquipmentTypesLocators
    }

    addEquipmentType(information){
        const info = information
        const maintenance = this.maintenance
        const equipmentTypesLocators = this.equipmentTypesLocators
        
        maintenance.addButton().click()
        equipmentTypesLocators.idInput().should('be.visible').wait(500).type(info.id)
        equipmentTypesLocators.descriptionInput().type(info.details.description)
        equipmentTypesLocators.classSelect().select(info.details.class)
        equipmentTypesLocators.fixedCostInput().click().clear().type(info.details.fixed_cost)
        equipmentTypesLocators.variableCostInput().click().clear().type(info.details.variable_cost)
        equipmentTypesLocators.heightInput().click().clear().type(info.details.height)
        equipmentTypesLocators.lengthInput().click().clear().type(info.details.lenght)
        equipmentTypesLocators.widthInput().click().clear().type(info.details.width)
        equipmentTypesLocators.weightInput().click().clear().type(info.details.weight)
        equipmentTypesLocators.IdlingFuelUsagePerHourInput().click().clear().type(info.details.fuel_usage)
        equipmentTypesLocators.powerUnitTypeRadio(info.details.power_unit_type).check().then(()=>{
            if(info.details.restricted_roads)
            equipmentTypesLocators.restructedCommertialRoadsCheckbox().check()
            else
            equipmentTypesLocators.restructedCommertialRoadsCheckbox().uncheck()
        })

        equipmentTypesLocators.saveButton().should('be.enabled').click()
        equipmentTypesLocators.closeButton().should('be.enabled').click()
    }

    editEquipmentType(information){
        const info = information
        const maintenance = this.maintenance
        const equipmentTypesLocators = this.equipmentTypesLocators
        let found

        maintenance.equipmentElementList().each(function ($element, $index) {
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement) => {
                if ($innerElement.text() == info.id) {
                    found = $index
                    return false
                }
            })
        }).then(() => {
            maintenance.equipmentElementList().eq(found).find('#editLink').click()
            equipmentTypesLocators.descriptionInput().clear().type(info.details.description)
            equipmentTypesLocators.classSelect().select(info.details.class)
            equipmentTypesLocators.fixedCostInput().click().clear().type(info.details.fixed_cost)
            equipmentTypesLocators.variableCostInput().click().clear().type(info.details.variable_cost)
            equipmentTypesLocators.heightInput().click().clear().type(info.details.height)
            equipmentTypesLocators.lengthInput().click().clear().type(info.details.lenght)
            equipmentTypesLocators.widthInput().click().clear().type(info.details.width)
            equipmentTypesLocators.weightInput().click().clear().type(info.details.weight)
            equipmentTypesLocators.IdlingFuelUsagePerHourInput().click().clear().type(info.details.fuel_usage)
            equipmentTypesLocators.powerUnitTypeRadio(info.details.power_unit_type).check().then(()=>{
                if(info.details.restricted_roads)
                equipmentTypesLocators.restructedCommertialRoadsCheckbox().check()
                else
                equipmentTypesLocators.restructedCommertialRoadsCheckbox().uncheck()
            })
            equipmentTypesLocators.saveButton().should('be.enabled').click()
        })
    }

    deleteEquipmentType(equipmentTypeId){
        const maintenance = this.maintenance
        let found = 0
        maintenance.equipmentElementList().each(function($element,$index){
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement)=>{
                if($innerElement.text() == equipmentTypeId){
                    found = $index
                }
            })
        }).then(()=>{
            maintenance.equipmentElementList().eq(found).find('#deleteLink').click().then(()=>{
                maintenance.deleteDialogYesButton().should('be.visible').click()
            })
        })
    }
}

export default EquipmentTypesMethods