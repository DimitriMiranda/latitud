class ServiceLocationLocators{
    //Add Window
    //Tabs
    addressTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Address")')
    }

    detailsTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Details")')
    }

    geocodeTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Geocode")')
    }

    sharedByRegionsTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Shared By Regions")')
    }

    //Address Tab

    idInput(){
        return cy.get('#identifier')
    }

    descriptionInput(){
        return cy.get('#description')
    }

    timeZoneSelect(){
        return cy.get('#timeZoneSelector')
    }

    locationCategorySelect(){
        return cy.get('#locationCategorySelector')
    }

    externalAddressInput(){
        return cy.get('#addressLine1')
    }

    internalAddressInput(){
        return cy.get('#addressLine2')
    }

    crossStreetInput(){
        return cy.get('#crossStreet')
    }

    postalCodeInput(){
        return cy.get('#postalCode')
    }

    countrySelect(){
        return cy.get('#countrySelector')
    }

    division1Input(){
        return cy.get('#adminDivision1')
    }

    division2Input(){
        return cy.get('#adminDivision2')
    }
    
    division3Input(){
        return cy.get('#adminDivision3')
    }

    //Details Tab
    mainContactFirstNameInput(){
        return cy.get('#contactFirstName')
    }

    mainContactLastNameInput(){
        return cy.get('#contactLastName')
    }

    mainContactPhoneNumber(){
        return cy.get('#contactPhoneNumber')
    }

    alternateContactFirstNameInput(){
        return cy.get('#alternateContactFirstName')
    }

    alternateContactLastNameInput(){
        return cy.get('#alternateContactLastName')
    }
    
    alternateContactPhoneNumber(){
        return cy.get('#alternateContactPhoneNumber')
    }

    //Import File
    importFileInput(){
        return cy.get('#file-input-importFileInput')
    }

    coordinateFormatSelect(){
        return cy.get('#coordinateFormatSelector')
    }


    //Save, Close and Import Buttons

    importButton(){
        return cy.get('button#importExecuteButton')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }

}

export default ServiceLocationLocators