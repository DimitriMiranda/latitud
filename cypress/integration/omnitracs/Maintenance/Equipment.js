import Login from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import MaintenanceEquipment from '../../../support/methods/maintenanceEquipment'

describe('Tests Equipment', function () {
    before(function () {
        //methods
        this.loginPage = new Login
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.equipment = new MaintenanceEquipment

        this.loginPage.visitLatitud()

        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
        })

        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule($navigation.maintenanceModule)
        })

        //Import
        cy.fixture('Maintenance/Equipment/importEquipment').then(function ($importDetails) {
            this.import = $importDetails
        })

        //Add
        cy.fixture('Maintenance/Equipment/addEquipment').then(function ($addInfo){
            this.addInfo = $addInfo
            
        })

        //Edit
        cy.fixture('Maintenance/Equipment/editEquipment').then(function ($equipmentInfo) {
            this.editInfo = $equipmentInfo
        })
           

    })

    it('Equipment functionalities (import, add, edit, delete)', function () {
        this.equipment.importFile(this.import.fileName)
        this.equipment.addEquipment(this.addInfo, true)
        this.equipment.editEquipment(this.editInfo)
        this.equipment.deleteEquipment(this.addInfo.id)
        this.customerPage.goHome()
    })
})
