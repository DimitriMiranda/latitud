import MaintenanceLocationLocators from '../pageObjects/maintenanceLocations'
import Maintenance from '../pageObjects/maintenance'

class MaintenanceLocations {
    constructor() {
        this.maintenance = new Maintenance
        this.maintenanceLocation = new MaintenanceLocationLocators
    }

    addMaintenanceLocation(information) { // Adds new maintenance location
        const maintenance = this.maintenance
        const maintenanceLocation = this.maintenanceLocation
        const info = information

        cy.on('uncaught:exception', function (err) {
            cy.log(err.message)
            return false
        })

        maintenance.addButton().click()
        maintenance.addButton().click() // This double click is because the uncaught error that is present
        maintenanceLocation.idInput().should('be.visible').wait(500).type(info.id)
        maintenanceLocation.timeZoneSelect().select(info.time_zone)
        maintenanceLocation.locationCategorySelect().select(info.location_category)
        maintenanceLocation.descriptionInput().type(info.description)

        maintenanceLocation.line1Input().type(info.address.line_1)
        maintenanceLocation.line2Input().type(info.address.line_2)
        maintenanceLocation.crossStreetInput().type(info.address.cross_street)
        maintenanceLocation.postalCode().type(info.address.postal_code)
        maintenanceLocation.countrySelect().select(info.address.country)

        maintenanceLocation.division1Input().type(info.address.division_1)
        maintenanceLocation.division2Input().type(info.address.division_2)
        maintenanceLocation.division3Input().type(info.address.division_3)

        maintenanceLocation.saveButton().should('be.enabled').click()
        maintenanceLocation.closeButton().click()
    }

    editMaintenanceLocation(information) { // Edits specific maintenance location using id
        const maintenanceLocation = this.maintenanceLocation
        const maintenance = this.maintenance
        const info = information

        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == info.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    maintenanceLocation.descriptionInput().clear().type(info.description)
                    maintenanceLocation.line1Input().clear().type(info.address.line_1)
                    maintenanceLocation.line2Input().clear().type(info.address.line_2)
                    maintenanceLocation.crossStreetInput().clear().type(info.address.cross_street)
                    maintenanceLocation.postalCode().clear().type(info.address.postal_code)
                    maintenanceLocation.countrySelect().select(info.address.country)

                    maintenanceLocation.division1Input().clear().type(info.address.division_1)
                    maintenanceLocation.division2Input().clear().type(info.address.division_2)
                    maintenanceLocation.division3Input().clear().type(info.address.division_3)

                    maintenanceLocation.saveButton().should('be.enabled').click()
                    return false
                }
            })
        })
    }

    importMaintenanceLocations(filename) { //Imports mainteinance locations using xlsx file
        const maintenance = this.maintenance
        const maintenanceLocation = this.maintenanceLocation

        maintenance.importButton().click()
        maintenanceLocation.filePathInput().attachFile('Files/' + filename)

        maintenanceLocation.importButton().should('be.enabled').wait(500).click()
        maintenanceLocation.closeButton().should('be.enabled').click()
    }

    deleteMaintenanceLocation(locationId) { // Deletes an specific maintenance location using id
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text().trim()
                cy.log(id)
                cy.log(locationId)
                if (id == locationId) {
                    cy.get($element).should('be.visible').wait(500).find('#deleteLink').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()

                    return false
                }
            })
        })
    }


}

export default MaintenanceLocations