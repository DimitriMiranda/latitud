class WorkersLocators{
    constructor(){}

    //Tabs
    workerDetailsTab(){
        return cy.get('#Worker-tabs li:contains("Worker Details") a')
    }

    workerCostsTab(){
        return cy.get('#Worker-tabs li:contains("Worker Costs") a')
    }

    complianceTab(){
        return cy.get('#Worker-tabs li:contains("Compliance") a')
    }

    sharedByRegionsTab(){
        return cy.get('#Worker-tabs li:contains("Shared By Regions") a')
    }


    workerIdInput(){
        return cy.get('#identifier')
    }

    colorDisplayButton(){
        return cy.get('#displayColor span i')
    }

    colorList(){
        return cy.get('#displayColor ul li')
    }

    workerTypeSelect(){
        return cy.get('#workerType')
    }

    workerFirstNameInput(){
        return cy.get('#firstName')
    }

    workerLastNameInput(){
        return cy.get('#lastName')
    }

    workerEquipmentSelect(){
        return cy.get('#equipment')
    }

    workerDepotSelect(){
        return cy.get('#depot')
    }

    workerMobileAppSelect(){
        return cy.get('#mobileApplicationTypeSelector')
    }

    workerContactPhInput(){
        return cy.get('#contactNumber')
    }

    mobileLoginInput(){
        return cy.get('#mobileLogin')
    }

    workerKeyFobInput(){
        return cy.get('#keyFobId')
    }    

    complianceCheckbox(){
        return cy.get('span:contains("Enable Driver Compliance")')
    }

    mobilePasswordInput(){
        return cy.get('#mobilePassword')
    }

    mobilePasswordConfirmationInput(){
        return cy.get('#mobilePasswordConfirm')
    }

    //Compliance Tab Locators
    licenseNumberInput(){
        return cy.get('#cdlNumber')
    }

    licenseCountrySelect(){
        return cy.get('#countrySelector')
    }

    licenseStateSelect(){
        return cy.get('#stateSelector')
    }

    //Shared to Regions Locators
    sharedToAllCheckbox(){
        return cy.get('#sharedToAll')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }

    importFileInput(){
        return cy.get('#file-input-importFileInput')
    }

    importFileButton(){
        return cy.get('#importExecuteButton')
    }

    importCloseButton(){
        return cy.get('#importCloseButton')
    }

}

export default WorkersLocators