import LoginPage from "../../../support/methods/loginPage"
import CustomerPage from "../../../support/methods/customerPage"
import MaintenanceWorkers from "../../../support/methods/maintenanceWorkers"
import MaintenanceModule from "../../../support/methods/maintenance"


describe('RPE-9548',function(){
    it('Add Driver',function(){
        const loginMethods = new LoginPage
        const customerMethods = new CustomerPage
        const maintenanceMethods = new MaintenanceModule
        const workersMethods = new MaintenanceWorkers

        loginMethods.visitLatitud('PROD')
        loginMethods.loginLatitud('dimitri.miranda@omnitracs.com','Roadnet@Mikau1401')
        customerMethods.selectCustomer('OT1-HOS3 Sync test 01')
        customerMethods.accessModule('Maintenance')
        maintenanceMethods.accessToModule('Workers')
        cy.fixture('Issues/RPE-9548/driverInfo').then(function($driverInfo){
            workersMethods.addWorker($driverInfo)
        })

        //Comment
        




    })
})