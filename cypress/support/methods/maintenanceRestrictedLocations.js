import Maintenance from '../pageObjects/maintenance'
import RestrictedLocationsLocators from '../pageObjects/restrictedLocations'

class RestrictedLocationsMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.restrictedLocations = new RestrictedLocationsLocators
    }

    addRestrictedLocation(information){
        const info = information
        const maintenance = this.maintenance
        const restrictedLocations = this.restrictedLocations

        maintenance.addButton().click()
        restrictedLocations.idInput().should('be.visible').wait(500).type(info.id)
        restrictedLocations.timeZoneSelect().select(info.time_zone)
        restrictedLocations.descriptionInput().type(info.description)
        restrictedLocations.addressLine1Input().type(info.address.line_1)
        restrictedLocations.addressLine2Input().type(info.address.line_2)
        restrictedLocations.crossStreetInput().type(info.address.cross_street)
        restrictedLocations.postalCodeinput().type(info.address.postal_code)
        restrictedLocations.countrySelect().select(info.address.country)
        restrictedLocations.adminDivision1Input().type(info.address.division_1)
        restrictedLocations.adminDivision2Input().type(info.address.division_2)
        restrictedLocations.adminDivision3Input().type(info.address.division_3)

        restrictedLocations.saveButton().should('be.enabled').click()
        restrictedLocations.closeButton().should('be.enabled').click()
    }

    editRestrictedLocation(information) { // Edits specific restricted location using id
        const restrictedLocations = this.restrictedLocations
        const maintenance = this.maintenance
        const info = information

        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text()
                if (id == info.id) {
                    cy.get($element).find('#editLink').should('be.visible').click()
                    restrictedLocations.descriptionInput().clear().type(info.description)
                    restrictedLocations.addressLine1Input().clear().type(info.address.line_1)
                    restrictedLocations.addressLine2Input().clear().type(info.address.line_2)
                    restrictedLocations.crossStreetInput().clear().type(info.address.cross_street)
                    restrictedLocations.postalCodeinput().clear().type(info.address.postal_code)
                    restrictedLocations.countrySelect().select(info.address.country)

                    restrictedLocations.adminDivision1Input().clear().type(info.address.division_1)
                    restrictedLocations.adminDivision2Input().clear().type(info.address.division_2)
                    restrictedLocations.adminDivision3Input().clear().type(info.address.division_3)

                    restrictedLocations.saveButton().should('be.enabled').click()
                    return false
                }
            })
        })
    }

    deleteRestrictedLocation(locationId) { // Deletes an specific restricted location using id
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function ($element) {
            cy.get($element).find('span.entity-name').eq(0).then(function ($innerElement) {
                let id = $innerElement.text().trim()
                cy.log(id)
                cy.log(locationId)
                if (id == locationId) {
                    cy.get($element).should('be.visible').wait(500).find('#deleteLink').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()

                    return false
                }
            })
        })
    }

}

export default RestrictedLocationsMethods