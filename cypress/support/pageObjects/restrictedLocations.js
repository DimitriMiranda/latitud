class RestrictedLocationsLocators{
    //Add Window
    idInput(){
        return cy.get('#identifier')
    }

    timeZoneSelect(){
        return cy.get('#timeZoneSelector')
    }

    locationCategorySelect(){
        return cy.get('#locationCategorySelector')
    }

    descriptionInput(){
        return cy.get('#description')
    }

    //Tabs
    addressTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Address")')
    }

    geocodeTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Geocode")')
    }

    sharedByRegionsTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Shared By Regions")')
    }

    //Address tab locators

    addressLine1Input(){
        return cy.get('#addressLine1')
    }

    addressLine2Input(){
        return cy.get('#addressLine2')
    }

    crossStreetInput(){
        return cy.get('#crossStreet')
    }

    postalCodeinput(){
        return cy.get('#postalCode')
    }

    countrySelect(){
        return cy.get('#countrySelector')
    }

    adminDivision1Input(){
        return cy.get('#adminDivision1')
    }

    adminDivision2Input(){
        return cy.get('#adminDivision2')
    }

    adminDivision3Input(){
        return cy.get('#adminDivision3')
    }


    //Save and Close buttons
    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }











}

export default RestrictedLocationsLocators