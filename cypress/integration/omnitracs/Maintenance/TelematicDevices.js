import Login from '../../../support/methods/loginPage'
import Customer from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import TelematicDevicesMethods from '../../../support/methods/maintenanceTelematicDevices'

describe('Telematic Devices Tests',function(){
    before('Login and move to module',function(){
        this.login = new Login
        this.customer = new Customer
        this.maintenance = new MaintenanceModule
        this.telematicDevices = new TelematicDevicesMethods
        

        cy.fixture('Credentials/customerLatitud').then(function($credentials){
            this.login.visitLatitud()
            this.login.loginLatitud($credentials.username,$credentials.password)
        })

        cy.fixture('Navigation/equipment').then(function($equipment){
            this.customer.selectCustomer($equipment.customer)
            this.customer.accessModule($equipment.module)
            this.maintenance.accessToModule('Telematics Devices')
        })

        cy.fixture('Maintenance/TelematicDevices/addInfo').then(function($addInfo){
            this.addInfo = $addInfo
        })

        cy.fixture('Maintenance/TelematicDevices/editInfo').then(function($editInfo){
            this.editInfo = $editInfo
        })
    })

    it('Add, Edit Telematic Device',function(){
        // this.telematicDevices.addTelematicDevice(this.addInfo)
        // cy.wait(3000)
        this.telematicDevices.importTelematicDevices('TelematicDevices.xlsx')
    })
})
