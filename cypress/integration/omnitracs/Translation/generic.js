/// <reference types="Cypress" />

import Login from '../../../support/methods/loginPage.js'
import Customer from '../../../support/methods/customerPage.js'

describe('Validate translations',function(){

    var loginPage = new Login
    var customerPage = new Customer
    var dictionary 

    beforeEach(() => {

        //Validation to detect the Environment Variable 'Language'
        if(Cypress.env('lang') == "es"){
            dictionary = "Languages/spanish"
        } if(Cypress.env('lang') == "fr"){
            dictionary = "Languages/french"
        } if(Cypress.env('lang') == "pt"){
            dictionary = "Languages/portuguese"
        }

        //Fixtures
        //LoginPage
        cy.fixture(dictionary).then(function($language){
            this.language = $language
        })
        cy.fixture('Credentials/customerLatitud').then(function($credentials){
            this.user = $credentials.userDH
            this.pass = $credentials.passDH
        })

        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
        })

    })

    it('Login Page',function(){

        loginPage.visitLatitud()
        loginPage.verifyTitle(this.language.loginPage.welcomeMessage)

    })

    it('Welcome Page',function(){

        loginPage.visitLatitud()
        loginPage.loginLatitud(this.user, this.pass)  
        customerPage.validateCustomerMessage(this.language.loginPage.changeCustomer)
    
    })

    it('Modules Page',function(){

        loginPage.visitLatitud()
        loginPage.loginLatitud(this.user, this.pass)  
        customerPage.selectCustomer(this.navigation.translations.customer)
        customerPage.validateModulesTranslated(this.language) 

      })


})