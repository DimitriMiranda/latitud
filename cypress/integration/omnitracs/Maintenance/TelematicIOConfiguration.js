import LoginPage from '../../../support/methods/loginPage'
import TelematicsIOConfigurationMethods from '../../../support/methods/maintenanceTelematicsIOConfigurartion'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'

describe('Test Suite', function () {
    before('Login and nav to module',function(){
        this.login = new LoginPage
        this.customer = new CustomerPage
        this.maintenance = new MaintenanceModule
        this.telematicsConfiguration = new TelematicsIOConfigurationMethods 
     
        cy.fixture('Credentials/customerLatitud').then(function($credentials){
            this.login.visitLatitud()
            this.login.loginLatitud($credentials.username,$credentials.password)
        })

        cy.fixture('Navigation/equipment').then(function($nav){
            this.customer.selectCustomer($nav.customer)
            this.customer.accessModule($nav.module)
            this.maintenance.accessToModule('Telematics Device I/O Configuration')
        })

        cy.fixture('Maintenance/TelematicIOConfiguration/addInfo').then(function($addInfo){
            this.addInfo = $addInfo
        })

        // cy.fixture('Maintenance/TelematicIOAccesories/editInfo').then(function($editInfo){
        //     this.editInfo = $editInfo
        // })
    })

    it('Test Telematic IO Accesories', function () {
        this.telematicsConfiguration.addTelematicsIoConfiguration(this.addInfo)
        
    })
})