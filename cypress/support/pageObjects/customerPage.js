class Customer{
    constructor(){}
    //Initial Page  ------------------------------------------------------------------------------------------------------

    customerSelect(){
        return cy.get('#SelectedCustomerEntityKey')
    }

    changeCustomer(){
        return cy.get('#changeCustomer')
    }

    homeLogo(){
        return cy.get('.logo-image > img')
    }

    optionsGearLink(){
        return cy.get('a#dropDownMenuButton')
    }

    gearOption(option){
        return cy.get('ul.dropdown-menu.navbar-dropdown-menu-right li:contains("'+option+'")')
    }

    changeCustomerMessage(){
        return cy.get('h3')
    }

    allModules(){
        return cy.xpath('//li/descendant::li/descendant::div[@class="title"]')
    }

    //Modules  ------------------------------------------------------------------------------------------------------    
    moduleList(){
        return cy.get('.component-list li:visible')
    }

}

export default Customer