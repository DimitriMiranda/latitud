import Maintenance from "../pageObjects/maintenance"
import TelematicIOAccesoriesLocators from "../pageObjects/telematicIOAccessories"

class TelematicIOAccesoriesMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.telematicAccesories = new TelematicIOAccesoriesLocators
    }

    addTelematicDeviceAccesory(info){
        this.maintenance.addButton().click()
        this.telematicAccesories.typeSelect().should('be.visible').wait(500).select(info.type)
        this.telematicAccesories.idInput().type(info.id)
        this.telematicAccesories.descriptionInput().type(info.description)
        if(info.type=='Binary Sensor'){
            this.telematicAccesories.binarySensorInputBiasSelect().select(info.binary_sensor.input_bias)
            this.telematicAccesories.binarySensorVoltageHighLabel().type(info.binary_sensor.voltage_high)
            this.telematicAccesories.binarySensorVoltageLowLabel().type(info.binary_sensor.voltage_low)
        }        
        this.telematicAccesories.saveButton().should('be.enabled').click()
        this.telematicAccesories.closeButton().should('be.enabled').click()
    }

    editTelematicDeviceAccessory(information){
        const info = information
        const telematicAccesories = this.telematicAccesories
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==info.id){
                    cy.get($element).find('#editLink').should('be.visible').click()
                    telematicAccesories.typeSelect().should('be.visible').wait(500).select(info.type)
                    telematicAccesories.descriptionInput().clear().wait(500).type(info.description)
                    telematicAccesories.saveButton().click()       
                }
            })
        })
    }

    deleteTelematicDeviceAccessory(informatitelematicDeviceAccesoryId){
       
        const maintenance = this.maintenance
        maintenance.equipmentElementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==informatitelematicDeviceAccesoryId){
                    cy.get($element).find('#deleteLink').should('be.visible').click()
                    maintenance.deleteDialogYesButton().should('be.visible').click()
                      
                }
            })
        })
    }
}

export default TelematicIOAccesoriesMethods //The export line must be in order to be able to import the class