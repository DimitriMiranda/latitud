import PingfedLocators from '../../pageObjects/pingfed'

class PingfedMethods {
    constructor(){
        this.pingLocators = new PingfedLocators
    }
    loginToPingFed(username,password){
        this.pingLocators.userIdInput().type(username)
        this.pingLocators.nextButton().click()
        this.pingLocators.passwordInput().type(password)
        this.pingLocators.loginButton().click()
       
    }
}

export default PingfedMethods