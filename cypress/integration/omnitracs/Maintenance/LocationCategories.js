import Login from '../../../support/methods/loginPage'
import Customer from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import LocationCategoriesMethods from '../../../support/methods/maintenanceLocationcategories'

describe('Equipment Types', function () {
    before('Login and move to module', function () {
        this.login = new Login
        this.customer = new Customer
        this.maintenance = new MaintenanceModule
        this.locationCategories = new LocationCategoriesMethods


        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.login.visitLatitud()
            this.login.loginLatitud($credentials.username, $credentials.password)
        })

        cy.fixture('Navigation/equipment').then(function ($equipment) {
            this.customer.selectCustomer($equipment.customer)
            this.customer.accessModule($equipment.module)
            this.maintenance.accessToModule('Location Categories')
        })

        cy.fixture('Maintenance/LocationCategories/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

        cy.fixture('Maintenance/LocationCategories/editInfo').then(function ($editInfo) {
            this.editInfo = $editInfo
        })
    })

    it('Test Location Categories', function () {
        this.locationCategories.addLocationCategory(this.addInfo)
        cy.wait(3000).log("Location Category added")
        this.locationCategories.editLocationCategory(this.editInfo)
        cy.wait(3000).log("Location Category modified")
        this.locationCategories.deleteLocationCategory(this.editInfo.id)
        cy.log("Location Category deleted")

    })
})