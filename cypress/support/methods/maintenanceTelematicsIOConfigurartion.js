import Maintenance from "../pageObjects/maintenance"
import TelematicsIOConfigurartionLocators from "../pageObjects/telematicsIOConfiguration"

class TelematicsIOConfigurationMethods{
    constructor(){
        this.maintenance = new Maintenance
        this.telematicsIoConfiguration = new TelematicsIOConfigurartionLocators
    }

    addTelematicsIoConfiguration(information){
        const info = information
        const maintenance = this.maintenance
        const telematicsIoConfiguration = this.telematicsIoConfiguration

        maintenance.addButton().click()
        telematicsIoConfiguration.idInput().should('be.visible').wait(500).type(info.id)
        telematicsIoConfiguration.descriptionInput().type(info.description)

        //Inputs
        if(info.inputs.port1!=null){
            telematicsIoConfiguration.inputPort1Select().find('option').contains(info.inputs.port1).then(function($element){
                telematicsIoConfiguration.inputPort1Select().select($element.attr('value'))
            })
        }

        if(info.inputs.port2!=null){
            telematicsIoConfiguration.inputPort2Select().find('option').contains(info.inputs.port2).then(function($element){
                telematicsIoConfiguration.inputPort2Select().select($element.attr('value'))
            })
        }

        if(info.inputs.port3!=null){
            telematicsIoConfiguration.inputPort3Select().find('option').contains(info.inputs.port3).then(function($element){
                telematicsIoConfiguration.inputPort3Select().select($element.attr('value'))
            })
        }

        if(info.inputs.port4!=null){
            telematicsIoConfiguration.inputPort4Select().find('option').contains(info.inputs.port4).then(function($element){
                telematicsIoConfiguration.inputPort4Select().select($element.attr('value'))
            })
        }

        if(info.inputs.port5!=null){
            telematicsIoConfiguration.inputPort5Select().find('option').contains(info.inputs.port5).then(function($element){
                telematicsIoConfiguration.inputPort5Select().select($element.attr('value'))
            })
        }

        if(info.inputs.port6!=null){
            telematicsIoConfiguration.inputPort6Select().find('option').contains(info.inputs.port6).then(function($element){
                telematicsIoConfiguration.inputPort6Select().select($element.attr('value'))
            })
        }

        this.telematicsIoConfiguration.outputsTab().click()

        //Outputs
        if(info.outputs.port1!=null){
            telematicsIoConfiguration.outputPort1Select().find('option').contains(info.outputs.port1).then(function($element){
                telematicsIoConfiguration.inputPort1Select().select($element.attr('value'))
            })
        }

        if(info.outputs.port2!=null){
            telematicsIoConfiguration.outputPort2Select().find('option').contains(info.outputs.port2).then(function($element){
                telematicsIoConfiguration.inputPort2Select().select($element.attr('value'))
            })
        }

        if(info.outputs.port3!=null){
            telematicsIoConfiguration.outputPort3Select().find('option').contains(info.outputs.port3).then(function($element){
                telematicsIoConfiguration.inputPort3Select().select($element.attr('value'))
            })
        }

        if(info.outputs.port4!=null){
            telematicsIoConfiguration.outputPort4Select().find('option').contains(info.outputs.port4).then(function($element){
                telematicsIoConfiguration.inputPort4Select().select($element.attr('value'))
            })
        }

        telematicsIoConfiguration.saveButton().should('be.enabled').click()
        telematicsIoConfiguration.closeButton().should('be.enabled').click()
    }
}

export default TelematicsIOConfigurationMethods