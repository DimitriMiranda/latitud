import Maintenance from '../pageObjects/maintenance'
import EquipmentLocators from '../pageObjects/equipment'

class MaintenanceModule {
    constructor() {
        this.maintenanceLocators = new Maintenance
        this.equipmentLocators = new EquipmentLocators
    }

    accessToModule(module) {
        this.maintenanceLocators.currentStep().click()
        const modules = this.maintenanceLocators.getModules()
        modules.each(($element, $index) => {
            const moduleText = $element.text()
            if (moduleText === module) {
                modules.eq($index).click({ force: true })
            }
        })
    }
}

export default MaintenanceModule