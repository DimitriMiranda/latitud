import CustomerPage from '../../../support/methods/customerPage'
import Login from '../../../support/methods/loginPage'
import PingfedMethods from '../../../support/methods/PingFed/pingfed'

describe('',function(){

    it('Latitude - SSO Logout',function(){
        let login = new Login
        let pingMethods = new PingfedMethods
        let customerMethods = new CustomerPage

        login.visitLatitud()
        login.goToPingFederated()
        pingMethods.loginToPingFed('hos3interactive2@foo.com','P@ssw0rd!')
        customerMethods.selectGearMenuOption('Logout')
        cy.title().then(function($title){
            expect($title).to.be.equal('Omnitracs')
        })
    })
})