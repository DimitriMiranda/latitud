import MaintenanceModule from '../../../support/methods/maintenance'
import LoginPage from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import RestrictedLocationsMethods from '../../../support/methods/maintenanceRestrictedLocations'

describe('Maintenance Location Tests',function(){
    before('Login latitud',function(){  
        
        //methods
        this.loginPage = new LoginPage
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.restrictedLocations = new RestrictedLocationsMethods
    
        cy.clearCookies()
        this.loginPage.visitLatitud()
        
    
        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
           cy.get('body').then(($body)=>{ // This is a test code to avoid the walk me pop up
               if($body.find('path')>0){
                   cy.get('path').click()
               }
           })
            
        })
    
        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Restricted Locations')
        })
    
        //Add Service Location
        cy.fixture('Maintenance/MaintenanceLocation/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

    })

    it('Test Restricted locations',function(){
        this.restrictedLocations.addRestrictedLocation(this.addInfo)
        cy.wait(1000)
        this.restrictedLocations.editRestrictedLocation(this.addInfo)
        cy.wait(1000)
        this.restrictedLocations.deleteRestrictedLocation(this.addInfo.id)
    })  
})