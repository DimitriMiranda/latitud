class TelematicsIOConfigurartionLocators{
    idInput(){
        return cy.get('input#identifier')
    }

    descriptionInput(){
        return cy.get('input#description')
    }
    //Tabs
    inputsTabs(){
        return cy.get('ul#inputsoutputs-tabs li:contains("Inputs") a')
    }

    outputsTab(){
        return cy.get('ul#inputsoutputs-tabs li:contains("Outputs") a')
    }

    //InputsTab
    inputPort1Select(){
        return cy.get('select#inputPort1')
    }

    inputPort2Select(){
        return cy.get('select#inputPort2')
    }

    inputPort3Select(){
        return cy.get('select#inputPort3')
    }

    inputPort4Select(){
        return cy.get('select#inputPort4')
    }

    inputPort5Select(){
        return cy.get('select#inputPort5')
    }

    inputPort6Select(){
        return cy.get('select#inputPort6')
    }

    //OutputsTab
    outputPort1Select(){
        return cy.get('select#outputPort1')
    }

    outputPort2Select(){
        return cy.get('select#outputPort2')
    }

    outputPort3Select(){
        return cy.get('select#outputPort3')
    }

    outputPort4Select(){
        return cy.get('select#outputPort4')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }


}

export default TelematicsIOConfigurartionLocators