/// <reference types="Cypress" />

import Login from '../../../Pages/LoginPage.js'
import Customer from '../../../Pages/CustomerPage.js'

describe('Validate translation to Spanish',function(){

    var loginPage = new Login
    var customerPage = new Customer

    /*it('Login Page',function(){

      console.log(Cypress.env('lang'))
      loginPage.verifyTitle('Bienvenido')
    
    })
    */

   it('Login Page',function(){

    cy.fixture('Languages/objects').then(function($lan){
      cy.log($lan.languages.spanish.welcomeMessage)
      loginPage.verifyTitle($lan.languages.spanish.welcomeMessage)
      
    })

  })


    it('Welcome Page',function(){

      cy.fixture('Credentials/customerLatitud').then(function($credentials){
          loginPage.loginLatitud($credentials.username, $credentials.password)  
          customerPage.validateCustomerMessage("Cambiar cliente")
      
      })
    
    })


})