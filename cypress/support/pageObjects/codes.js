class CodesLocators{
    idInput(){
        return cy.get('input#identifier')
    }

    descriptionInput(){
        return cy.get('input#description')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }
}

export default CodesLocators