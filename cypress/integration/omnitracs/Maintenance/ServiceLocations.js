
import Login from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import ServiceLocations from '../../../support/methods/maintenanceServiceLocations'

describe('Test Service Location',function(){
    before(function () {
        //methods
        this.loginPage = new Login
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.serviceLocations = new ServiceLocations
    
        this.loginPage.visitLatitud()
    
        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
        })
    
        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            this.maintenancePage.accessToModule('Service Locations')
        })
    
        //Add Service Location
        cy.fixture('Maintenance/ServiceLocation/addServiceLocation').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

    })

    it('Add Service Location',function(){
        // this.serviceLocations.addServiceLocation(this.addInfo)
        this.serviceLocations.importServiceLocationFile('ServiceLocations.xlsx')
        
    })
})
