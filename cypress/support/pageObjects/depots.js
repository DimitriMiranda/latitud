class DepotsLocators{
    idInput(){
        return cy.get('#identifier')
    }

    timeZoneDepotSelect(){
        return cy.get('#timeZoneSelector')
    }

    locationCategorySelect(){
        return cy.get('#locationCategorySelector')
    }

    descriptionInput(){
        return cy.get('#description')
    }

    //-------------------------------------Tabs--------------------------------------------------

    addressTab(){
        return cy.get('#Depot-tabs li:contains("Address")')
    }

    detailsTab(){
        return cy.get('#Depot-tabs li:contains("Details")')
    }

    geocodeTab(){
        return cy.get('#Depot-tabs li:contains("Geocode")')
    }

    openCloseTimesTab(){
        return cy.get('#Depot-tabs li:contains("Open Close Times")')
    }

    sharedByRegionsTab(){
        return cy.get('#Depot-tabs li:contains("Shared By Regions")')
    }

    //---------------------------------------------------------------------------------------------

    //Address tab fields

    line1Input(){
        return cy.get('#addressLine1') //External address
    }

    line2Input(){
        return cy.get('#addressLine2') //Internal address
    }

    division1Input(){
        return cy.get('#adminDivision1') // State
    }

    division2Input(){
        return cy.get('#adminDivision2') //Mexico -> City / USA -> Country
    }

    division3Input(){
        return cy.get('#adminDivision3') //Mexico -> Settlement / USA -> City
    }

    postalCodeInput(){
        return cy.get('#postalCode')
    }

    countrySelect(){
        return cy.get('#countrySelector')
    }

    //Details Tab
    detailsPhone(){
        return cy.get('#phoneNumber')
    }

    detailsAlternatePhone(){
        return cy.get('#alternatePhoneNumber')
    }

    detailsFaxNumber(){
        return cy.get('#faxNumber')
    }



    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }






   

    
















}

export default DepotsLocators