import RegionLocators from '../../pageObjects/Administration/regions'
class RegionsMethods {
    constructor(){
        this.regions = new RegionLocators
    }

    addRegion(){
        this.regions.addButton().click()

        
    }

    editRegion(information){
        const info = information
        const regions = this.regions

        regions.elementList().each(function($element){
            cy.get($element).find('span.entity-name').eq(0).then(function($innerElement){
                let id = $innerElement.text()
                if(id==info.id){
                    cy.get($element).find('#editLink').click()
                    //Code to edit a Region
                }
            })           
        })




    }
}

export default RegionsMethods