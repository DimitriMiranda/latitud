
/// <reference types="Cypress" />

class Login{  

    usernameInput(){
        return cy.get('input#UserName')     
    }

    passwordInput(){
        return cy.get('input#Password')
    }

    submitButton(){
        return cy.get('button#submitButton')
    }

    welcomeMessage(){
        return cy.get('h3.text-center')
    }

    optionsButton(){
        return cy.get('#dropDownMenuButton > .fa')
    }

    loginLink(){
        return cy.get('#loginLink')
    }

    federatedLoginLink(){
        return cy.get('a:contains("Omnitracs Federated Login")')
    }
}

export default Login