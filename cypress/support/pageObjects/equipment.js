class EquipmentLocators{
    equipmentInputId(){
        return cy.get('#identifier')
    }

    //Details Tab
    equipmentChecxBoxActiveEquipment(){
        return cy.get('#activeEquipment')
    }

    equipmentInputDescription(){
        return cy.get('#description')
    }

    equipmentInputLicenceNumber(){
        return cy.get('#licensePlateNumber')
    }

    equipmentInputVin(){
        return cy.get('#vin')
    }

    equipmentSelectPreferredWorker(){
        return cy.get('#preferredWorker')
    }

    equipmentSelectEquipmentType(){
        return cy.get('#equipmentType')
    }

    equipmentSelectBuddyEquipment(){
        return cy.get('#buddyEquipment')
    }

    equipmentTelematicDeviceInput(){
        return cy.get('#telematicsDeviceSelector')
    }

    telematicElementList(){
        return cy.get('ul[id*="ui-id"] li')
    }

    selectVehicleClass(){
        return cy.get('#equipmentClass')
    }

    inputFixedCost(){
        return cy.get('#fixedCost input')
    }

    inputVariableCost(){
        return cy.get('#variableCost input')
    }

    inputHeight(){
        return cy.get('#height input')
    }

    inputLength(){
        return cy.get('#length input')
    }

    inputWidth(){
        return cy.get('#width input')
    }

    inputWeight(){
        return cy.get('#weight input')
    }

    inputFuelPerHour(){
        return cy.get('#IdlingFuelUsagePerHour input')
    }

    radioPowerVehicle(){
        return cy.get('#powerUnitVehicle')
    }

    radioPowerTrailer(){
        return cy.get('#powerUnitTrailer')
    }

    checkboxRestrictedRoads(){
        return cy.get('#restrictedCommercialRoads')
    }

    addEquipmentTabs(){
        return cy.get('ul#EquipmentType-tabs li a')
    }

    //Shared By Regions Tab
    checkboxSharedToAllRegions(){
        return cy.get('#sharedToAll')
    }

    inputFilePath(){
        return cy.get('#file-input-importFileInput')
    }

    importFileButton(){
        return cy.get('#importExecuteButton')
    }

    importCloseButton(){
        return cy.get('#importCloseButton')
    }

    resultsWin(){
        return cy.get('h3').contains('Results')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }

}

export default EquipmentLocators