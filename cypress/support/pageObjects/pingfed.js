class PingfedLocators {
    userIdInput(){
        return cy.get('input[name="user-id"]')
    }

    nextButton(){
        return cy.get('button:contains("Next")')
    }

    passwordInput(){
        return cy.get('input[name="password"]')
    }

    loginButton(){
        return cy.get('button:contains("Login")')
    }
}

export default PingfedLocators