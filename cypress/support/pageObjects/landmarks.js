class LandmarksLocators{
     // Add Window
    //Tabs
    addressTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Address")')
    }

    detailsTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Details")')
    }

    geocodeTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Geocode")')
    }

    sharedByRegionsTab(){
        return cy.get('#ServiceLocation-tabs li:contains("Shared By Regions")')
    }

    idInput(){
        return cy.get('#identifier')
    }

    timeZoneSelect(){
        return cy.get('#timeZoneSelector')
    }

    locationCategorySelect(){
        return cy.get('#locationCategorySelector')
    }

    descriptionInput(){
        return cy.get('#description')
    }

    //Address tab

    line1Input(){
        return cy.get('#addressLine1')
    }

    line2Input(){
        return cy.get('#addressLine2')
    }

    crossStreetInput(){
        return cy.get('#crossStreet')
    }

    postalCodeInput(){
        return cy.get('#postalCode')
    }

    countrySelect(){
        return cy.get('#countrySelector')
    }

    division3Input(){
        return cy.get('#adminDivision3')
    }

    division2Input(){
        return cy.get('#adminDivision2')
    }

    division1Input(){
        return cy.get('#adminDivision1')
    }

    //Details Tab

    mainContactFirstNameInput(){
        return cy.get('#contactFirstName')
    }

    mainContactLastNameInput(){
        return cy.get('#contactLastName')
    }

    mainContactPhoneNumber(){
        return cy.get('#contactPhoneNumber')
    }

    alternateContactFirstNameInput(){
        return cy.get('#alternateContactFirstName')
    }

    alternateContactLastNameInput(){
        return cy.get('#alternateContactLastName')
    }
    
    alternateContactPhoneNumber(){
        return cy.get('#alternateContactPhoneNumber')
    }

    standardInstructionsInput(){
        return cy.get('#standardInstructions')
    }

    //Import window
    filePathInput(){
        return cy.get('#file-input-importFileInput')
    }

    coordinateSelect(){
        return cy.get('#coordinateFormatSelector')
    }

    //Import, Save and Close buttons

    importButton(){
        return cy.get('#importExecuteButton')
    }

    saveButton(){
        return cy.get('button:contains("Save"):visible')
    }

    closeButton(){
        return cy.get('button:contains("Close"):visible')
    }

}

export default LandmarksLocators