import Login from '../../../support/methods/loginPage'
import Customer from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import WorkerTypesMethods from '../../../support/methods/maintenanceWorkerTypes'

describe('Equipment Types', function () {
    before('Login and move to module', function () {
        this.login = new Login
        this.customer = new Customer
        this.maintenance = new MaintenanceModule
        this.workerTypes = new WorkerTypesMethods


        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.login.visitLatitud()
            this.login.loginLatitud($credentials.username, $credentials.password)
        })

        cy.fixture('Navigation/equipment').then(function ($equipment) {
            this.customer.selectCustomer($equipment.customer)
            this.customer.accessModule($equipment.module)
            this.maintenance.accessToModule('Worker Types')
        })

        cy.fixture('Maintenance/WorkerTypes/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })

        cy.fixture('Maintenance/WorkerTypes/editInfo').then(function ($editInfo) {
            this.editInfo = $editInfo
        })
    })

    it('Test Worker Type', function () {
        this.workerTypes.addWorkerType(this.addInfo)
        cy.log('Worker type added').then(function () {
            this.workerTypes.editWorkerType(this.editInfo)
        })

        cy.log("Worker type edited").wait(3000).then(function () {
            this.workerTypes.deleteWorkerType(this.editInfo.id)
        })

        cy.log("Worker type deleted")

    })
})