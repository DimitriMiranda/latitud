
import Customer from '../pageObjects/customerPage'

class CustomerPage{
    constructor(){
        this.customerLocators = new Customer
    }

    goHome(){
        this.customerLocators.homeLogo().click()
    }

    selectGearMenuOption(option){
        this.customerLocators.optionsGearLink().click()
        this.customerLocators.gearOption(option).click()
    }

    selectCustomer(customer){
        this.customerLocators.customerSelect().should('be.visible').select(customer)
        this.customerLocators.changeCustomer().click()
    }

    accessModule(module){        
        this.customerLocators.moduleList().each(($element, $index)=>{
            const text = $element.text()
            if(text.includes(module)){
                this.customerLocators.moduleList().eq($index).find('a').click({force:true})

            }
        })
    }

    validateCustomerMessage(message){
        this.customerLocators.changeCustomerMessage().should('include.text', message)
    }

    validateModulesTranslated(language){
        this.customerLocators.allModules().each(function($title){
            const module = $title.text()
            const expected = Object.values(language.ModulesPage)  
            let verifyAssert = false     
                
            for(let i=0; i<expected.length; i++){
                const english = Object.keys(language.ModulesPage)[i]
                if(module==expected[i]){
                    assert.equal(module, expected[i], "Original label: " + english)
                    verifyAssert = true
                }
            }
            if(verifyAssert == false){
                assert.fail("No translations found for: " + module)
            }
        })
    }

}

export default CustomerPage