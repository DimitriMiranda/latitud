import ExceptionRulesLocators from "../pageObjects/exceptionRules"
import Maintenance from "../pageObjects/maintenance"

class ExceptionRulesMethods{
    constructor(){
        this.maintenace = new Maintenance
        this.exceptionRules = new ExceptionRulesLocators
    }

    addExceptionRule(information){
        const info = information
        const maintenace = this.maintenace
        const exceptionRules = new ExceptionRulesLocators

        maintenace.addButton().click()
        exceptionRules.idInput().should('be.visible').wait(500).type(info.general.id)
        exceptionRules.ruleTypeSelect().select(info.rules.rule_type)
        if(info.general.description!=null)
            exceptionRules.descriptionInput().clear().wait(500).type(info.general.description)

        exceptionRules.displayColorButton().click()
        exceptionRules.colorList().eq(5).click()
        if(info.general.request_acknoledgement){
            exceptionRules.requireAcknowledgementCheckbox().scrollIntoView().check()
        }else{
            exceptionRules.requireAcknowledgementCheckbox().scrollIntoView().uncheck()
        }

        if(info.general.is_enabled){
            exceptionRules.enabledCheckbox().scrollIntoView().check()
        }else{
            exceptionRules.enabledCheckbox().scrollIntoView().uncheck()
        }

        switch (info.rules.rule_type) {
            case 'Device Low Battery':
                exceptionRules.lowBatteryThresholdInput().clear().wait(500).type(info.rules.low_battery_threshold)
                break;

            case 'GPS Gap':
                exceptionRules.maxDistanceWithoutGPSInput().clear().wait(500).type(info.rules.gps_gap)
                break;

            case 'Idling'://Problem with the input with two fields
                exceptionRules.idlingDurationInput().clear().wait(500).click().type(info.rules.idling_time)
                break;

            case 'Off Planned Route Time':
                exceptionRules.outOfPlanDurationInput().clear().wait(500).click().type(info.rules.off_plan_time)
                break;
            
            case 'Out Of Bounds':
                exceptionRules.outOfBoundsInput().clear().wait(500).type(info.rules.out_of_bounds_distance)
                break;

            case 'Out Of Contact':
                exceptionRules.outOfContactInput().clear().wait(500).click().type(info.rules.device_out_of_contact_time)
                break;

            case 'Posted Road Speed Violation':
                exceptionRules.speedViolationMilesToleranceInput().clear().type(info.rules.speed_tolerance.miles_per_hour)
                exceptionRules.speedViolationDurationInput().clear().wait(500).click().type(info.rules.speed_tolerance.duration)
                break;

            case 'Projected Missed Service Window':
                exceptionRules.earlyMissedServiceWindowMinutesInput().clear().wait(500).type(info.rules.missed_service_window.early_window)
                exceptionRules.lateMissedServiceWindowMinutesInput().clear().wait(500).type(info.rules.missed_service_window.late_window)
                if(info.rules.missed_service_window.ignore_early_arrivals)
                    exceptionRules.missedWindowIgnoreEarlyArrivalsInput().check()
                else
                    exceptionRules.missedWindowIgnoreEarlyArrivalsInput().uncheck()
                break;

            case 'Restricted Equipment Hours':
                exceptionRules.restrictedEquipmentHoursModeSelect().select(info.rules.restricted_equipment_mode)
                break;
            
            case 'Route Path Deviation':
                exceptionRules.routePathDeviationMilesInput().clear().wait(500).type(info.rules.route_path_deviation_miles)
                break;

            case 'Service Time Deviation':
                exceptionRules.serviceTimeDeviationVarianceMinutes().clear().wait(500).type(info.rules.service_time_deviation.time_variance_minutes)
                exceptionRules.serviceTimeDeviationPercentage().clear().wait(500).type(info.rules.service_time_deviation.time_variance_percentage)
                break;

            case 'Telematics Accessory':
                //Objects missing for the checkboxes
                cy.log('Missing Telematics Accesory implementation')
                break;   
            
            case 'Unplanned Stop':
                exceptionRules.uplannedStopMaximumValueInput().clear().wait(500).click().type(info.rules.unplanned_stop_time)
                break;

            case 'User Defined Speed Violation':
                exceptionRules.userDefinedSpeedViolationMaxSpeedInput().clear().wait(500).type(info.rules.user_defined_speed_violation.maximum_speed_allowed)
                exceptionRules.userDefinedSpeedViolationMinDurationInput().clear().wait(500).click().type(info.rules.user_defined_speed_violation.minimum_speed_duration)
                break;

            case 'User Defined':
                //Missing logic for this case
                cy.log('Missing implementation for user defined')
                break;

            default:
                break;
        }

        exceptionRules.saveButton().should('be.enabled').click()
        exceptionRules.closeButton().should('be.enabled').click()

    }
}

export default ExceptionRulesMethods