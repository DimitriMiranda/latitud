import Login from '../pageObjects/loginPage'

class LoginPage{
    constructor(){
        this.loginLocators = new Login
    }

    visitLatitud(environment){
        cy.visit(Cypress.env(environment+'_URL'))
    }

    loginLatitud(username, password){
        this.loginLocators.usernameInput().type(username)
        this.loginLocators.passwordInput().type(password)
        this.loginLocators.submitButton().click()        
    }

    verifyTitle(message){
        this.loginLocators.optionsButton().click()
        this.loginLocators.loginLink().click()
        this.loginLocators.welcomeMessage().should(function($welcomeMessage){
            expect($welcomeMessage.text()).to.contain(message)
        })
    }

    goToPingFederated(){
        this.loginLocators.federatedLoginLink().click()
        cy.title().then(function($title){
            cy.log($title)
        })
    }
}

export default LoginPage