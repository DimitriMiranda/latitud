import Maintenance from "../pageObjects/maintenance"
import TelematicDevicesLocators from '../pageObjects/telematicDevices'
class TelematicDevicesMethods {
    constructor() {
        this.maintenance = new Maintenance
        this.telematicDevices = new TelematicDevicesLocators
    }

    addTelematicDevice(information) {
        const maintenance = this.maintenance
        const telematicDevices = this.telematicDevices
        const info = information

        maintenance.addButton().click()
        telematicDevices.telematicIDInput().should('be.visible').wait(500).type(info.general.id)
        if (info.general.active) {
            telematicDevices.activeDeviceCheckbox().check({ force: true })
        } else {
            telematicDevices.activeDeviceCheckbox().uncheck({ force: true })
        }

        if (info.general.thirdParty) {
            telematicDevices.thirdPartyDeviceCheckbox().check()
            telematicDevices.telematicProviderDeviceSelect().select(info.general.platform)
        } else {
            telematicDevices.thirdPartyDeviceCheckbox().uncheck()
            telematicDevices.hardwarePlatformSelect().select(info.general.platform)
        }

        telematicDevices.telematicDescriptionInput().type(info.general.description)

        //Applications
        let platform = info.general.platform
        if (info.general.thirdParty && (platform == 'Omnitracs') && info.applications != null) {
            telematicDevices.applicationsTab().click()
            //Here should be the code for the applications tab
        }

        telematicDevices.saveButton().should('be.enabled').click()
        telematicDevices.closeButton().should('be.enabled').click()
    }

    editTelematicDevice(information) {
        const info = information
        const maintenance = this.maintenance
        const telematicDevices = this.telematicDevices
        let found = 0

        maintenance.equipmentElementList().each(function ($element, $index) {
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement) => {
                let id = $innerElement.text()
                if (id == info.general.id) {
                    found = $index
                    return false
                }
            })
        }).then(() => {
            maintenance.equipmentElementList().eq(found).find('#editLink').click()
            telematicDevices.telematicDescriptionInput().clear().wait(500).type(info.general.description).then(() => {
                if (info.general.active) {
                    telematicDevices.activeDeviceCheckbox().check()
                } else {
                    telematicDevices.activeDeviceCheckbox().uncheck()
                }
            })
            telematicDevices.saveButton().should('be.enabled').click()
        })
    }

    deleteTelematicDevice(telematicDeviceId){
        const maintenance = this.maintenance
        let found = 0

        maintenance.equipmentElementList().each(function($element,$index){
            cy.get($element).find('span.entity-name').eq(0).then(($innerElement)=>{
                if($innerElement.text() == telematicDeviceId){
                    found = $index
                }
            })
        }).then(()=>{
            maintenance.equipmentElementList().eq(found).find('#deleteLink').click().then(()=>{
                maintenance.deleteDialogYesButton().should('be.visible').click()
            })
        })
    }

    importTelematicDevices(filename){
        const maintenance = this.maintenance
        const telematicDevices = this.telematicDevices

        maintenance.importButton().click()
        telematicDevices.importFileInput().attachFile('Files/'+filename)
        telematicDevices.importButton().should('be.enabled').click()
    }
}

export default TelematicDevicesMethods