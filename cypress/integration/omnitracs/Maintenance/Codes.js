import Login from '../../../support/methods/loginPage'
import CustomerPage from '../../../support/methods/customerPage'
import MaintenanceModule from '../../../support/methods/maintenance'
import CodesMethods from '../../../support/methods/maintenanceCodes'

describe('Tests Depots', function () {
    before(function () {
        //new methods
        this.loginPage = new Login
        this.customerPage = new CustomerPage
        this.maintenancePage = new MaintenanceModule
        this.codes = new CodesMethods

        this.loginPage.visitLatitud()

        //fixtures
        //Credentials
        cy.fixture('Credentials/customerLatitud').then(function ($credentials) {
            this.loginPage.loginLatitud($credentials.username, $credentials.password)
        })

        //Navigation
        cy.fixture('Navigation/equipment').then(function ($navigation) {
            this.navigation = $navigation
            this.customerPage.selectCustomer($navigation.customer)
            this.customerPage.accessModule($navigation.module)
            // this.maintenancePage.accessToModule('Route Break Reason codes')
            // this.maintenancePage.accessToModule('Route Tender Reason Codes')
            this.maintenancePage.accessToModule('Stop Cancel Reason Codes')
            // this.maintenancePage.accessToModule('Unserviceable Stop Reason Codes')
            // this.maintenancePage.accessToModule('Quantity Reason Codes')
            // this.maintenancePage.accessToModule('Order Cancel Reason Codes')
        })

        //Add Depot
        cy.fixture('Maintenance/Codes/addInfo').then(function ($addInfo) {
            this.addInfo = $addInfo
        })


    })

    it('Depots functionalities (add, edit, delete)', function () {
        this.codes.addCode(this.addInfo)
        cy.wait(3000)
        this.codes.editCode(this.addInfo)
        cy.wait(3000)
        this.codes.deleteCode(this.addInfo.id)
    })
})
